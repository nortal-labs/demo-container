#!/bin/bash

PROJECT_ID=observability-deep-dive-europe
VERSION=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')
CONTAINER_NAME=demo-container

echo "Building Container"

# Docker build
docker build -t eu.gcr.io/${PROJECT_ID}/${CONTAINER_NAME}:${VERSION} .

docker push eu.gcr.io/${PROJECT_ID}/${CONTAINER_NAME}:${VERSION}

echo ""
echo "Run Locally If needed"
echo ""
echo "Version: ${VERSION}"
echo ""
echo "docker run --rm -p 8080:8080 eu.gcr.io/${PROJECT_ID}/${CONTAINER_NAME}:${VERSION}"
