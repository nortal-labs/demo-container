package com.clickconcepts;

import java.util.ArrayList;
import java.util.List;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.netty.util.internal.shaded.org.jctools.util.RangeUtil;

@Controller("/")
public class DescribeEnvironmentController {

    private static final String KEY_PREFIX = "DEMO";

    private final MeterRegistry meterRegistry;

    DescribeEnvironmentController(final MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Produces(MediaType.TEXT_HTML)
    @Get("/envvars")
    public String index() {
        meterRegistry
                .counter("metric_name", "label_key", "label_value")
                .increment();

        List<Number> list = getRandomList();
        Gauge.builder("users_logged_in", list, List::size)
        .description("Simulate a user logging in") // optional
        // .tags("region", "test") // optional
        .register(meterRegistry);

        StringBuffer output = new StringBuffer("<html><head>TEST</head><body>");
        System.getenv().forEach((key, value) -> {
            if(key.startsWith(KEY_PREFIX)) {
                String strippedKey = key.replace(KEY_PREFIX, "").toString();
                output.append(strippedKey).append("=").append(value).append("<br/>");
            }
        });
        
        return output.append("</body></html>").toString();
    }
    
    @Produces(MediaType.TEXT_HTML)
    @Get("/")
    public String hello() {
        
        return "<html><head>HOME</head><body><h3>Welcome</h3><p><a href=\"/envvars\">Environment Variables</a></p></body></html>";
    }
    
    private static double getRandomDoubleBetweenRange(double min, double max){
        double x = (Math.random()*((max-min)+1))+min;
        return x;
    }

    private List<Number> getRandomList() {
        List<Number> list = new ArrayList<Number>();
        int rand = (int) Math.round(getRandomDoubleBetweenRange(0, 100));
        for(int i=0; i<rand; i++) {
            list.add(1);
        }
        return list;
    }
}